import 'package:flutter/material.dart';
import 'package:my_app/routing_constants.dart';
import './router.dart' as router;
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);
  final isLogin = true;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'FlutterApp',
      onGenerateRoute: router.myRoute,
      initialRoute: InfoViewRoute,
      debugShowCheckedModeBanner: false,
    );
  }
}