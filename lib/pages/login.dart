import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  final String argument;
  const LoginScreen({Key key, this.argument}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: Text('LoginScreen $argument')),
    );
  }
}
