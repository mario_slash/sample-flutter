import 'package:flutter/material.dart';
import 'package:my_app/pages/login.dart';
import 'package:my_app/pages/home.dart';
import 'package:my_app/pages/info.dart';
import 'package:my_app/routing_constants.dart';

Route<dynamic> myRoute(RouteSettings settings) {
  switch (settings.name) {
    case InfoViewRoute:
      return MaterialPageRoute(builder: (content) => InfoScreen());
    case HomeViewRoute:
      return MaterialPageRoute(builder: (content) => HomeScreen());
    case LoginViewRoute:
      var argument = settings.arguments;
      return MaterialPageRoute(builder: (content) => LoginScreen(argument: argument,));
    default:
      return MaterialPageRoute(builder: (content) => HomeScreen());
  }
}